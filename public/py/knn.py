
import cv2
import joblib
import sys


def predict(file_name):
    model = joblib.load('py/KM.pkl')
    img = cv2.imread(f'storage/1204/{file_name}')
    img = cv2.resize(img, (1920, 1080), interpolation=cv2.INTER_CUBIC)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(img, (5, 5), 0)
    canny = cv2.Canny(blurred, 30, 150)
    flatten_img = canny.flatten()
    result = model.predict([flatten_img])
    return result[0]

if __name__ == '__main__':
    param = sys.argv
    print(predict(param[1]))
