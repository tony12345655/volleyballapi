
import numpy as np
import cv2
from keras.models import load_model
import sys

def predict(file_name):
    model = load_model('py/cnn.h5')
    img = cv2.imread(f'storage/1204/{file_name}')
    img = cv2.resize(img, (624, 416), interpolation=cv2.INTER_CUBIC)
    img_arr = np.array([img])
    img = img_arr.reshape((img_arr.shape[0], 624, 416, 3)).astype('float32')
    img /= 255
    predict = model.predict(img)
    result = np.argmax(predict, axis=1)
    return result[0]


if __name__ == '__main__':
    param = sys.argv
    print(predict(param[1]))