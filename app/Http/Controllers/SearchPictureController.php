<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pictures;
use Storage;

class SearchPictureController extends Controller
{
    public function index(Request $request){
        
        $tag = $request -> input('tag');
        $pictures_name = Pictures::where('tag', 'like', '%'.$tag.'%') -> get('name');
        $name = [];
        foreach ($pictures_name as $key => $value) {
            $name[$key] = $value -> name;
        }

        return $name;
    }

    public function store(Request $request){
        
        $file = $request -> file('file');
        // 取得檔案原名
        $file_name = $file->getClientOriginalName();
        // 儲存檔案並指訂名稱
        $file -> storeAs("public/1204/", $file_name);
        // knn模型給特徵
        exec("python py/knn.py {$file_name}", $out_km, $res_km);
        if ($out_km[0] == '0'){
            $tag = "比賽";
        }
        else{
            $tag = "休閒";
            exec("python py/cnn.py {$file_name}", $out_cnn, $res_cnn);
            if ($out_cnn[0] == '0'){
                $tag = $tag.",合照";
            }
            else if($out_cnn[0] == '1'){
                $tag = $tag.",個人照";
            }
            else{
                $tag = $tag.",其他";
            }
        }
        // 存入資料庫
        Pictures::create(['name' => $file_name, 'tag' => $tag]);
        $data = array('name' => $file_name, 'tag' => $tag);
        $data = json_encode($data);
        return $data;

    }
}
